// Оператор try-catch можно использовать, 
// когда вы хотите скрыть ошибки от пользователя или 
// когда хотите создать пользовательские ошибки в интересах ваших пользователей.

const root = document.getElementById('root');
const list = document.createElement('ul');

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];


books.forEach((el, index) => {
  const autorCheck = el.hasOwnProperty("author")
  const nameCheck = el.hasOwnProperty("name")
  const priceCheck = el.hasOwnProperty("price")
  try {
    if (autorCheck && nameCheck && priceCheck) {
      const listElement = document.createElement('li');
      listElement.innerHTML =
        `<p>${el.author}</p>
        <p>${el.name}</p>
        <p>${el.price}</p>`
      list.append(listElement)
    } else {
      const error = `error: object index - ${index} ${!autorCheck ? 'no autor property' : !nameCheck ? 'no name property' : 'no price property'}`
      throw error;
    };
  } catch (err) {
    console.log(err);
  }
})

root.append(list);